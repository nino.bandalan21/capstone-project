import { Component, OnDestroy, OnInit } from '@angular/core';
import { NbMediaBreakpointsService, NbMenuService, NbSidebarService, NbThemeService, NbMenuItem } from '@nebular/theme';

import { UserData } from '../../../@core/data/users';
import { LayoutService, AnalyticsService } from '../../../@core/utils';
import { map, takeUntil, filter } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { NavController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { environment } from '../../../../environments/environment'

@Component({
  selector: 'ngx-header',
  styleUrls: ['./header.component.scss'],
  templateUrl: './header.component.html',
})
export class HeaderComponent implements OnInit, OnDestroy {
  constructor(private sidebarService: NbSidebarService,
    private menuService: NbMenuService,
    private themeService: NbThemeService,
    private layoutService: LayoutService,
    private breakpointService: NbMediaBreakpointsService,
    private nav: NavController,
    private storage: Storage,
    private analyticsService: AnalyticsService) {
  }

  public destroy$: Subject<void> = new Subject<void>();

  public themes = [
    {
      value: 'default',
      name: 'Light',
    },
    {
      value: 'dark',
      name: 'Dark',
    },
    {
      value: 'cosmic',
      name: 'Cosmic',
    },
    {
      value: 'corporate',
      name: 'Corporate',
    },
  ];

  public menu = [
    { title: 'Create New Admin', icon: 'person-add-outline', data: { id: 'register' } },
    { title: 'Change Password', icon: 'person', data: { id: 'change-password' } },
     { title: 'Log out', icon: 'log-out-outline', data: { id: 'logout' } },
  ];
  public user: any = {
    firstname: "",
    picture: ""
  } 

  ngOnInit() {
    this.menuService.onItemClick().subscribe(async (event) => {
      if (event.item.title === 'Log out') {
        await this.storage.clear();
        this.nav.navigateRoot("/login");
      }
      if (event.item.title === 'Create New Admin'){
        this.nav.navigateRoot("/register");
      }
      if (event.item.title === 'Change Password'){
        this.nav.navigateRoot("/profile");
      }
    });
    setInterval(() => this.storageGet(), 1);
  }

  storageGet(){
  const username = this.storage.get("username");
  const url = this.storage.get("url");
  Promise.all([username]).then((arrayOfResults) => {
    this.user.firstname = arrayOfResults[0];
  });
  Promise.all([url]).then((arrayOfResults) => {
    this.user.picture = arrayOfResults[0];
  });
}

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  changeTheme(themeName: string) {
    this.themeService.changeTheme(themeName);
  }

  toggleSidebar(): boolean {
    this.sidebarService.toggle(true, 'menu-sidebar');
    this.layoutService.changeLayoutSize();
    return false;
  }

  navigateHome() {
    this.nav.navigateRoot("/dashboard");
  }
}


