import { ExtraOptions, RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { AuthGuardGuard } from './guards/guard/auth-guard.guard';

const routes: Routes = [
  { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
  {

    path: 'login',
    loadChildren: () => import('./pages/login/login.module').then(m => m.LoginPageModule),
  },
  {
    path: 'dashboard',
    loadChildren: () => import('./pages/dashboard/dashboard.module').then(m => m.DashboardPageModule),
    canActivate: [AuthGuardGuard]
  },
  {
    path: 'notification',
    loadChildren: () => import('./pages/notification/notification.module').then(m => m.NotificationPageModule),
    canActivate: [AuthGuardGuard]
  },
  {
    path: 'puv',
    loadChildren: () => import('./pages/puv/puv.module').then(m => m.PuvPageModule),
    canActivate: [AuthGuardGuard]
  },
  {
    path: 'puv/add-puv',
    loadChildren: () => import('./pages/add-puv/add-puv.module').then(m => m.AddPuvPageModule),
    canActivate: [AuthGuardGuard]
  },
  {
    path: 'puv/update-puv/:id',
    loadChildren: () => import('./pages/update-puv/update-puv.module').then(m => m.UpdatePuvPageModule),
    canActivate: [AuthGuardGuard]
  },
  {
    path: 'notification/update-notification/:id',
    loadChildren: () => import('./pages/update-notification/update-notification.module').then( m => m.UpdateNotificationPageModule),
    canActivate: [AuthGuardGuard]
  },
  {
    path: 'register',
    loadChildren: () => import('./pages/register/register.module').then( m => m.RegisterPageModule),
    canActivate: [AuthGuardGuard]
  },
  {
    path: 'puv/view-puv/:id',
    loadChildren: () => import('./pages/view-puv/view-puv.module').then( m => m.ViewPuvPageModule),
    canActivate: [AuthGuardGuard]
  },
  {
    path: 'history',
    loadChildren: () => import('./pages/history/history.module').then( m => m.HistoryPageModule),
    canActivate: [AuthGuardGuard]
  },
  {
    path: 'profile',
    loadChildren: () => import('./pages/profile/profile.module').then( m => m.ProfilePageModule),
    canActivate: [AuthGuardGuard]
  },  {
    path: 'testing',
    loadChildren: () => import('./pages/testing/testing.module').then( m => m.TestingPageModule)
  },







];

const config: ExtraOptions = {
  useHash: false,
};

@NgModule({
  imports: [RouterModule.forRoot(routes, config)],
  exports: [RouterModule],
})
export class AppRoutingModule {
}
