import { Component, OnInit } from '@angular/core';

import { Platform, Events } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { NbMenuItem } from '@nebular/theme';
import { Config } from '918kiss2u-sdk';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit {
  constructor(private platform: Platform
    , private splashScreen: SplashScreen
    , private statusBar: StatusBar
    , private events: Events
  ) { }

  public menu: Array<NbMenuItem> = [
    {
      title: 'Dashboard',
      icon: 'home',
      link: '/dashboard',
    },
    {
      title: 'PUV',
      icon: 'car',
      link: '/puv',
    },
    {
      title: 'Notification',
      icon: 'bell',
      link: '/notification',
    },
    {
      title: 'History',
      icon: 'book-open-outline',
      link: '/history',
    },
  ];
  public has_menu: boolean = false;
  public has_header: boolean = false;

  ngOnInit() {
    Config.BASE_URL = environment.API_URL;
    this.init();
  }

  init() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();

      this.events.subscribe("has_menu", has_menu => {
        this.has_menu = has_menu;
      });
      this.events.subscribe("has_header", has_header => {
        this.has_header = has_header;
      });
    });
  }
}
