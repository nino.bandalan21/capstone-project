import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'status'
})
export class StatusPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    let result = "";
    switch(value.toUpperCase()){
      case "CREATED":
          result = "requested"
        break;
      case "IN_PROGRESS":
          result = "pending"
        break;
      case "SUCCESS":
          result = "success"
        break;
      case "DECLINED":
          result = "declined"
        break;
    }
    return result;
  }

}
