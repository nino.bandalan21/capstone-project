import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { HttpClientModule } from '@angular/common/http';

import { DashboardPageRoutingModule } from './dashboard-routing.module';
import { DashboardPage } from './dashboard.page';
import { NbMenuModule, NbCardModule, NbUserModule, NbListModule, NbButtonModule } from '@nebular/theme';
import { ThemeModule } from '../../@theme/theme.module';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    HttpClientModule,
    DashboardPageRoutingModule,
    ThemeModule,
    NbMenuModule,
    NbCardModule,
    NbUserModule,
    NbListModule,
    NbButtonModule
  ],
  declarations: [DashboardPage]
})
export class DashboardPageModule {}
