import { Component, OnInit, ViewChild } from "@angular/core";
import {
  Events,
  NavController,
  ToastController,
  LoadingController
} from "@ionic/angular";
import { NotificationService } from "../../services/notification.service";
import * as moment from "moment";
import { AngularFireDatabase } from "@angular/fire/database";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { PagerService } from "../../services/pager.service";
import { MessageService } from "../../services/message.service";
import { Message } from "./notification";
import { Storage } from "@ionic/storage";
import { HistoryService } from '../../services/history.service';

@Component({
  selector: "app-notification",
  templateUrl: "./notification.page.html",
  styleUrls: ["./notification.page.scss"]
})
export class NotificationPage implements OnInit {
  message = "";
  user: any;
  messages: Observable<any[]>;
  notification: Array<any> = [];
  users: Array<any> = [];
  // message: Message = new Message();

  constructor(
    private pagerService: PagerService,
    private events: Events,
    private navController: NavController,
    private toast: ToastController,
    private notificationService: NotificationService,
    private loadingController: LoadingController,
    private db: AngularFireDatabase,
    private messageService: MessageService,
    private storage: Storage,
    private historyService: HistoryService
  ) { }

  loading: any;
  today: number = Date.now();

  // pager object
  pager: any = {};

  // paged items
  pagedItems: any[];

  ngOnInit() {
    this.events.publish("has_menu", true);
    this.events.publish("has_header", true);
    this.init();
    this.getMessageList();
    this.getUserList();
      setInterval(() => {
        this.today = Date.now();
      }, 1);
  }

  async init() {
    this.loading = await this.loadingController.create({
      spinner: "bubbles",
      duration: 10000
    });
    await this.loading.present();
    this.hideLoading();
  }

  //get all messages in the firebase
  getMessageList() {
    this.messageService
      .getMessageList()
      .snapshotChanges()
      .pipe(
        map(changes =>
          changes.map(c => ({ key: c.payload.key, ...c.payload.val() }))
        )
      )
      .subscribe(messages => {
        this.notification = messages.reverse();
        this.setPage(1);
      });
  }

  //get all users in the firebase
  getUserList() {
    this.messageService
      .getUserList()
      .snapshotChanges()
      .pipe(
        map(changes =>
          changes.map(c => ({ key: c.payload.key}))
        )
      )
      .subscribe(users => {
        this.users = users
      });
  }

  //sending notification to all users
  async save() {
    const username = this.storage.get("username");
    Promise.all([username]).then(arrayOfResults => {
      return (this.user = arrayOfResults[0]);
    });

    this.loading = await this.loadingController.create({
      spinner: "bubbles",
      duration: 10000
    });
    await this.loading.present();
    if (this.message) {
      this.hideLoading();
      let notif = this.messageService.createAdminMessage(
        this.message,
        this.user,
        this.today,
      );
      //loop all users then send notification
      for (let index = 0; index < this.users.length; index++) {
        const id = this.users[index].key;
          let notif = this.messageService.createUserMessage(
            this.message,
            id,
            this.today
          );
      }
      
       let _data = {
          action_type: 'Send-Notification',
          message: this.message,
          date: this.today
        }
        let history = await this.historyService.Save(_data)
        this.message = "";
        this.toastMessageSuccess("Send Notification Successful");
    } else {
      this.hideLoading();
      this.toastMessageError("Please fill in the field");
    }
  }

  private hideLoading() {
    this.loading.dismiss();
  }

  setPage(page: number) {
    // get pager object from service
    this.pager = this.pagerService.getPager(this.notification.length, page);

    // get current page of items
    this.pagedItems = this.notification.slice(
      this.pager.startIndex,
      this.pager.endIndex + 1
    );
  }

  async push() {
    this.save();
  }
  // async push() {
  //   if (this.message) {
  //     let data = {
  //       message: this.message
  //     }

  //     this.loading = await this.loadingController.create({
  //       spinner: 'bubbles',
  //       duration: 10000
  //     });
  //     await this.loading.present();

  //     let result: any = await this.notificationService.Save(data)
  //     if (!result.error && result.data) {
  //       this.hideLoading()
  //       this.init()
  //       this.message = ''
  //       this.toastMessageSuccess('Send Notification Successful')
  //     } else {
  //       this.toastMessageError(result.message)
  //     }
  //   } else {
  //     this.toastMessageError('Please check message and try again')
  //   }
  // }

  edit(item) {
    this.navController.navigateRoot(
      `/notification/update-notification/${item.key}`
    );
  }

  formatTime(date: string) {
    return moment(new Date(date)).format("MMM DD, YYYY h:mm a");
  }

  async toastMessageSuccess(message) {
    let toast = await this.toast.create({
      message: message,
      duration: 2000,
      position: "bottom",
      color: "success"
    });
    return toast.present();
  }
  async toastMessageError(message) {
    let toast = await this.toast.create({
      message: message,
      duration: 2000,
      position: "bottom",
      color: "danger"
    });
    return toast.present();
  }
}
