import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PuvPage } from './puv.page';

const routes: Routes = [
  {
    path: '',
    component: PuvPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PuvPageRoutingModule {}
