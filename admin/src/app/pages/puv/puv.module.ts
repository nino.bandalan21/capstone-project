import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { PagerService } from '../../services/pager.service';
import {NgxPaginationModule} from 'ngx-pagination'; // pagination

import { IonicModule } from '@ionic/angular';

import { PuvPageRoutingModule } from './puv-routing.module';
import { PuvPage } from './puv.page';
import { NbSelectModule, NbCardModule, NbButtonModule, NbIconModule, NbCheckboxModule, NbInputModule, NbDatepickerModule, NbTooltipModule } from '@nebular/theme';

@NgModule({
  imports: [
    CommonModule,
    NgxPaginationModule,
    NbSelectModule,
    FormsModule,
    IonicModule,
    NbCardModule,
    NbButtonModule,
    NbIconModule,
    NbCheckboxModule,
    NbInputModule,
    NbDatepickerModule,
    NbTooltipModule,
    PuvPageRoutingModule
  ],
  declarations: [PuvPage],
  providers: [
    PagerService
],
})
export class PuvPageModule {}
