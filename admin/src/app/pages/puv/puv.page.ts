import { Component, OnInit, ViewChild } from '@angular/core';
import { Events, NavController, ToastController, LoadingController } from '@ionic/angular';
import { VehicleService } from '../../services/vehicle.service';
import { PagerService } from '../../services/pager.service'


@Component({
  selector: 'app-puv',
  templateUrl: './puv.page.html',
  styleUrls: ['./puv.page.scss'],
})
export class PuvPage implements OnInit {

  constructor(
    private events: Events,
    private pagerService: PagerService,
    private navController: NavController,
    private vehicle: VehicleService,
    private toast: ToastController,
    private loadingController: LoadingController,

  ) {
  }

  private vehicles: any[];
  loading: any;

   // pager object
   pager: any = {};

   // paged items
   pagedItems: any[];

  ngOnInit() {
    this.events.publish("has_menu", true);
    this.events.publish("has_header", true);
    this.init()
  }
  create() {
    this.navController.navigateRoot("/puv/add-puv");
  }

  edit(item) {
    this.navController.navigateRoot(`/puv/update-puv/${item._id}`);
  }
  view(item) {
    this.navController.navigateRoot(`/puv/view-puv/${item._id}`);
  }

  async init() {
    this.loading = await this.loadingController.create({
      spinner: 'bubbles',
      duration: 10000,
      cssClass: 'transparent',
    });

    await this.loading.present();

    let result: any = await this.vehicle.Listing()
    if (!result.error && result.data) {
      this.hideLoading()
      this.vehicles = result.data

       // initialize to page 1
       this.setPage(1);

    } else {
      this.vehicles = null
      this.toastMessageError(result.message)
    }
  }

  setPage(page: number) {
    // get pager object from service
    this.pager = this.pagerService.getPager(this.vehicles.length, page);

    // get current page of items
    this.pagedItems = this.vehicles.slice(this.pager.startIndex, this.pager.endIndex + 1);
  }

  private hideLoading() {
    this.loading.dismiss();
  }

  async toastMessageSuccess(message) {
    let toast = await this.toast.create({
      message: message,
      duration: 2000,
      position: "bottom",
      color: 'success',
    });
    return toast.present();
  }
  async toastMessageError(message) {
    let toast = await this.toast.create({
      message: message,
      duration: 2000,
      position: "bottom",
      color: 'danger',
    });
    return toast.present();
  }

}