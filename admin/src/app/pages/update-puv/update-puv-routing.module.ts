import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UpdatePuvPage } from './update-puv.page';

const routes: Routes = [
  {
    path: '',
    component: UpdatePuvPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UpdatePuvPageRoutingModule {}
