import { Injectable } from '@angular/core';
import ServerConfig from '../configs/server.config';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) { }
  async Login(username: String, password: String) {
    let data = {
      username: username,
      password: password
    }
    return await this.http.post(`${ServerConfig.API}/api/auth/admin/login`, data).toPromise()
  }
  async Register(username: String, password: String, url : string) {
    let data = {
      username: username,
      password: password,
      url : url
    }
    return await this.http.post(`${ServerConfig.API}/api/auth/admin/register`, data).toPromise()
  }
}
