import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: "root"
})
export class GeocodingService {
  constructor(private http: HttpClient) {}

  async ReverseGeocode(lat: any, lng: any) {
    return await this.http.get(
      `https://maps.googleapis.com/maps/api/geocode/json?latlng=${lat},${lng}&key=${environment.GOOGLE_MAPS_KEY}`
    );
  }

}
