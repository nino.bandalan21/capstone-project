import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Storage } from '@ionic/storage';
import ServerConfig from '../configs/server.config';

@Injectable({
    providedIn: 'root'
})
export class NotificationService {

    constructor(private http: HttpClient,
        private storage: Storage) { }

    async Save(data) {
        let token = await this.storage.get('token')
        return await this.http.post(`${ServerConfig.API}/api/notification/save?access_token=${token}`, data).toPromise()
    }

    async Listing() {
        let token = await this.storage.get("token")
        return await this.http.get(`${ServerConfig.API}/api/notification/listing?access_token=${token}`).toPromise()
    }

    async GetById(id) {
        let token = await this.storage.get("token")
        return await this.http.get(`${ServerConfig.API}/api/notification/get/${id}?access_token=${token}`).toPromise()
      }
}
