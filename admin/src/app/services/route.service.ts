import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import ServerConfig from '../configs/server.config';
import { Storage } from '@ionic/storage';


@Injectable({
  providedIn: 'root'
})
export class RouteService {

  constructor(private http: HttpClient,
    private storage: Storage) { }

  async Save(data) {
    let token = await this.storage.get("token")
    return await this.http.post(`${ServerConfig.API}/api/route/save?access_token=${token}`, data).toPromise()
  }

  async Listing() {
    let token = await this.storage.get('token')
    return await this.http.get(`${ServerConfig.API}/api/route/listing?access_token=${token}`).toPromise()
  }

  async GetById(id) {
    let token = await this.storage.get("token")
    return await this.http.get(`${ServerConfig.API}/api/route/get/${id}?access_token=${token}`).toPromise()
  }
  
  async DeleteById(id){
    let token = await this.storage.get("token")
    return await this.http.delete(`${ServerConfig.API}/api/route/delete?access_token=${token}`).toPromise()
  }
}
