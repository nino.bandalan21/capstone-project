import { Component, OnInit } from '@angular/core';
import { MenuController, LoadingController, NavController, PopoverController } from '@ionic/angular';
import { PopoverComponent } from '../popover/popover.component';
import { Storage } from "@ionic/storage";
import { not } from '@angular/compiler/src/output/output_ast';
import { count } from 'rxjs/operators';
import { MessageService } from '../../../services/message.service'
import { map } from "rxjs/operators";
import { UsersService } from "../../../services/users.service";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  
  notifications: Array<any> = [];
  public id: any
  public length: any;
  public count = 0
  constructor(
    private menu:MenuController,
    private loading:LoadingController,
    private nav:NavController,
    private popoverController: PopoverController,
    private storage: Storage,
    private messageService: MessageService,
    private usersService: UsersService
    ) { }

  ngOnInit() {
      this.init()
  }

  async init(){
    this.getUsersList()

    // let _notifications: Array<any> = await this.storage.get("notifications");
    // this.length = _notifications.length
    // for (let index = 0, count = 0; index < _notifications.length; index++) {
    //   const element = _notifications[index];
    //   if (element.is_clicked === true){
    //     this.length--
    //     console.log(this.length)
    //   }
    // }
  }

  //Get All Users then check the current user 
  async getUsersList() {
    let result: any = await this.usersService.GetCurrent();
    let phone_number = result.data.owner.phone_number
    this.messageService
      .getUserList()
      .snapshotChanges()
      .pipe(
        map(changes =>
          changes.map(c => ({ key: c.payload.key, ...c.payload.val() }))
        )
      )
      .subscribe(users => {
        for (let index = 0; index < users.length; index++) {
          const element = users[index];
          if(element.phone_number != phone_number){
          }else {
            this.id = users[index].key
          }
        }
        this.getUserListById(this.id)
      });
  }

  //Get UserById to get all messages belong to the current User
  getUserListById(id) {
    this.messageService
    .getUserListByid(this.id)
    .snapshotChanges()
    .pipe(
      map(changes =>
        changes.map(c => ({ key: c.payload.key, ...c.payload.val() }))
      )
    )
    .subscribe(user_messages => {
      this.length = user_messages.length - 1
        for (let index = 0; index < user_messages.length - 1; index++) {
            this.notifications.push(user_messages[index])
        }
        if(this.length === this.notifications.length){
          for (let index = 0, count = 0; index < this.length; index++) {
            const element = this.notifications[index];
              if (element.is_clicked === false){
                count++
                this.count = count 
              }
          }
        }else {
          for (let index = this.notifications.length - this.length, count = 0; index < this.notifications.length; index++) {
            const element = this.notifications[index];
              if (element.is_clicked === true){
                count++
                this.count = this.length - count 
              }
              else{
  
              }
          }
        }  
    });
  }

  openFirst() {
    this.menu.enable(true, 'first');
    this.menu.open('first');
  }

  openEnd() {
    this.menu.open('end');
  }

  openCustom() {
    this.menu.enable(true, 'custom');
    this.menu.open('custom');
  }

  async notification() {
    let loading = await this.loading.create({
      message: "please wait...",
      mode: "ios",
      duration: 1500
    });
    loading.present();
    this.nav.navigateRoot("notification");
  }

  async home() {
    let loading = await this.loading.create({
      message: "please wait...",
      mode: "ios",
      duration: 1500
    });
    loading.present();
    this.nav.navigateRoot("home");
  }
  
  async presentPopover(ev: any) {
    const popover = await this.popoverController.create({
      component: PopoverComponent,
      event: ev,
      translucent: true
    });
    return await popover.present();
  }
 
}
