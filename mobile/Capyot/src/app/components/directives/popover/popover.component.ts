import { Component, OnInit } from '@angular/core';
import { Events, NavParams, PopoverController, NavController, LoadingController } from '@ionic/angular';
import { Storage } from "@ionic/storage";

@Component({
  selector: 'app-popover',
  templateUrl: './popover.component.html',
  styleUrls: ['./popover.component.scss'],
})
export class PopoverComponent implements OnInit {
  page;

  constructor(
    private events: Events,
    private navParams: NavParams,
    private popoverController: PopoverController,
    private nav:NavController,
    private loading:LoadingController,
    private storage:Storage){}

  ngOnInit() {
    //Get data from popover page
    this.page = this.navParams.get('data');
  }

  async logout() {
    await this.storage.set("token", null);
    let loading = await this.loading.create({
      message: "please wait...",
      mode: "ios",
      duration: 1500
    });
    this.nav.navigateRoot("login");
    this.popoverController.dismiss();
    loading.present();
  }

  myProfile() {
    this.popoverController.dismiss();
  }

}
