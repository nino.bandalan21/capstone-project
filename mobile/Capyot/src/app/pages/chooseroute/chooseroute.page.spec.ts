import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ChooseroutePage } from './chooseroute.page';

describe('ChooseroutePage', () => {
  let component: ChooseroutePage;
  let fixture: ComponentFixture<ChooseroutePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChooseroutePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ChooseroutePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
