import { Component, OnInit } from "@angular/core";
import { LoadingController, NavController } from "@ionic/angular";
import { Storage } from "@ionic/storage";
import { UsersService } from "../../services/users.service";

@Component({
  selector: "app-loading",
  templateUrl: "./loading.page.html",
  styleUrls: ["./loading.page.scss"]
})
export class LoadingPage implements OnInit {
  constructor(
    private loading: LoadingController,
    private nav: NavController,
    private storage: Storage,
    private userService: UsersService
  ) {}

  public loader: any;
  ngOnInit() {
    this.init();
  }

  async init() {
    this.loader = await this.loading.create({
      message: "Loading, please wait",
      mode: "ios",
      duration: 2000
    });
    this.loader.present();
    this.redirect("/login");

    // this is when we already have tokens in the application
    let token = await this.storage.get("token");
    if (token) {
      let user = await this.userService.GetCurrent()
      if (user) {
        this.redirect("/home");
      } else {
        this.redirect("/login");
      }
    } else {
      this.redirect("/login");
    }
  }

  redirect(url) {
    setTimeout(() => {
      this.loader.dismiss();
      this.nav.navigateRoot(url);
    }, 1000);
  }
}
