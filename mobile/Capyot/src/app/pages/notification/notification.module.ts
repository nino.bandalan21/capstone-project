import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { environment } from '../../../environments/environment';
import { IonicModule } from '@ionic/angular';
import { NotificationPageRoutingModule } from './notification-routing.module';
import { ComponentsModule } from '../../components/components.module';
import { NotificationPage } from './notification.page';
import { TimeAgoPipe } from 'time-ago-pipe';

@NgModule({
  imports: [
    CommonModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule, //for database
    FormsModule,
    IonicModule,
    ComponentsModule,
    NotificationPageRoutingModule
  ],
  declarations: [NotificationPage, TimeAgoPipe]
})
export class NotificationPageModule {}
