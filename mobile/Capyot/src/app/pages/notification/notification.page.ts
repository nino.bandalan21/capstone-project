import { Component, OnInit } from "@angular/core";
import { MessageService } from "../../services/message.service";
import { UsersService } from "../../services/users.service";
import { Message } from "./notification";
import { map } from "rxjs/operators";
import { Storage } from "@ionic/storage";
import { findWhere, indexOf } from "underscore";
import { NavController, LoadingController } from '@ionic/angular';

@Component({
  selector: "app-notification",
  templateUrl: "./notification.page.html",
  styleUrls: ["./notification.page.scss"]
})
export class NotificationPage implements OnInit {

  notification = [];
  public id: any
  
  constructor(
    private messageService: MessageService,
    private usersService: UsersService,
    private loading:LoadingController,
    private nav:NavController,
    private storage: Storage,
    private navController: NavController,
  ) {}

  ngOnInit() {
    this.getUsersList();
  }

  //get specific user and specific messages
  getUserListById(id) {
    let result: Array<any> =[]
    this.messageService
      .getUserListByid(id)
      .snapshotChanges()
      .pipe(
        map(changes =>
          changes.map(c => ({ key: c.payload.key,...c.payload.val() }))
        )
      )
      .subscribe(user_messages => {
        //reverse so that the new notification will come first
        this.notification = user_messages.reverse()
      });
  }


  //get all users
 async getUsersList() {
    let result: any = await this.usersService.GetCurrent();
    let phone_number = result.data.owner.phone_number
    this.messageService
      .getUserList()
      .snapshotChanges()
      .pipe(
        map(changes =>
          changes.map(c => ({ key: c.payload.key, ...c.payload.val() }))
        )
      )
      .subscribe(users => {
        for (let index = 0; index < users.length; index++) {
          const element = users[index];
          if(element.phone_number != phone_number){
          }else {
            this.id = users[index].key
            this.getUserListById(users[index].key)
          }
        }
      });
  }

  // async toLocalStorage(notifications) {
  //   let _notifications: Array<any> = await this.storage.get("notifications");
  //   if (_notifications && _notifications.length > 0) {
  //     let indexes = notifications.length - _notifications.length;
  //     let i = notifications.length - 1
  //     while (indexes > 0) {
  //       let data = {
  //         key: notifications[i].key,
  //         message: notifications[i].message,
  //         is_clicked: false
  //       };
  //       _notifications.push(data)
  //       indexes--
  //     }
  //     await this.storage.set("notifications", _notifications);
  //   } else {
  //     let __notifications: Array<any> = [];
  //     notifications.forEach(item => {
  //       let data = {
  //         key: item.key,
  //         message: item.message,
  //         is_clicked: false
  //       };
  //       __notifications.push(data);
  //     });
  //     await this.storage.set("notifications", __notifications);
  //   }
  //   this.notification = await this.storage.get("notifications");
  // }

  //to update the is_clicked to the firebase
  async view(item) {
    let loading = await this.loading.create({
      message: "please wait...",
      mode: "ios",
      duration: 5000
    });
    loading.present();
    this.navController.navigateRoot(`/view-notification/${item.key}`);
    let result: any = await this.messageService.updateUserMessage(item.key, this.id, item.message);
   
    // let notifications = await this.storage.get("notifications");
    // item.is_clicked = true;
    // for (let index = 0; index < notifications.length; index++) {
    //   if (notifications[index].message === item.message) {
    //     notifications[index].is_clicked = true;
    //   }
    // }
    // await this.storage.set("notifications", notifications);
  }
}
