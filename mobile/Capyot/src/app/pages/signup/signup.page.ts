import { Component, OnInit } from "@angular/core";
import {
  NavController,
  LoadingController,
  ToastController
} from "@ionic/angular";
import { AuthService } from "../../services/auth.service";
import { Storage } from "@ionic/storage";
import { MessageService } from 'src/app/services/message.service';

@Component({
  selector: "app-signup",
  templateUrl: "./signup.page.html",
  styleUrls: ["./signup.page.scss"]
})
export class SignupPage implements OnInit {
  public phone_number: any = "";

  constructor(
    private messageService: MessageService,
    private nav: NavController,
    private loading: LoadingController,
    private authService: AuthService,
    private toast: ToastController,
    private storage: Storage
  ) {}

  ngOnInit() {}

  async send_otpLoading() {
    let loading = await this.loading.create({
      message: "Sending Code...",
      mode: "ios",
      duration: 1500
    });
    loading.present();
  }

  async signUp() {
    if(this.phone_number){
      let data = {
        phone_number: this.phone_number
      };
      let result: any = await this.authService.Register(data);
  
      //save registered User in the Firebase
      let notif = this.messageService.createUser(
        this.phone_number,
      );
      if (!result.error && result.data) {
        await this.storage.set("token", result.data.token.token);
        await this.send_otpLoading();
        this.nav.navigateRoot("send-otp");
      } else {
        this.toastMessage(result.message);
      }
    }else{
      this.toastMessage('Please fill in the field');
    }
 
  }

  async toastMessage(message) {
    let toast = await this.toast.create({
      message: message,
      duration: 2000,
      position: "middle",
      mode: "ios"
    });
    return toast.present();
  }

  async login(){
    let loading = await this.loading.create({
      message: "Please wait...",
      mode: "ios",
      duration: 1500
    });
    loading.present();
    this.nav.navigateRoot("login");
  }
}
