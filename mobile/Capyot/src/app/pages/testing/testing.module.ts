import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";

import { IonicModule } from "@ionic/angular";

import { TestingPageRoutingModule } from "./testing-routing.module";

import { TestingPage } from "./testing.page";
import { AgmCoreModule } from "@agm/core";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TestingPageRoutingModule,
    AgmCoreModule.forRoot({
      apiKey: "AIzaSyDDJy6V5orDgjpC1RO3lZqwYsCqQQm0u6s"
    })
  ],
  declarations: [TestingPage]
})
export class TestingPageModule {}
