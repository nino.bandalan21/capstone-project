import { Component, OnInit, NgZone } from "@angular/core";
import { MapsAPILoader } from "@agm/core";

@Component({
  selector: "app-testing",
  templateUrl: "./testing.page.html",
  styleUrls: ["./testing.page.scss"]
})
export class TestingPage implements OnInit {
  public lat: any;
  public lng: any;
  public zoom: any = 16;

  constructor(private ngZone: NgZone) {}

  public query: any;

  ngOnInit() {
    this.init();
  }

  async init() {
    if (navigator) {
      console.log(navigator);
      navigator.geolocation.getCurrentPosition(pos => {
        console.log(pos.coords);
        this.lng = pos.coords.longitude;
        this.lat = pos.coords.latitude;
      });
    } else {
      console.log("walay navigator");
    }
  }

  search() {
    let defaultBounds = new google.maps.LatLngBounds(
      new google.maps.LatLng(10.402755, 123.763448),
      new google.maps.LatLng(10.310471, 123.92528)
    );
    let nativeHomeInputBox = document
      .getElementById("searchInput")
      .getElementsByTagName("input")[0];
    let autocomplete = new google.maps.places.Autocomplete(nativeHomeInputBox, {
      types: ["establishment"],
      bounds: defaultBounds
    });
    autocomplete.addListener("place_changed", () => {
      this.ngZone.run(() => {
        const place: google.maps.places.PlaceResult = autocomplete.getPlace();
        // this.query = place["formatted_address"];
        console.log(this.query);
      });
    });
  }
}
