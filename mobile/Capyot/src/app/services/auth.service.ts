import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Storage } from "@ionic/storage";
import { ServerConfig } from "../configs/server.config";

@Injectable({
  providedIn: "root"
})
export class AuthService {
  constructor(private http: HttpClient, private storage: Storage) {}

  async Login(phone_number) {
    let userCredentials = {
      phone_number: phone_number
    };
    let _user = await this.http
      .post(`${ServerConfig.API}/api/auth/login`, userCredentials)
      .toPromise();
    return _user;
  }

  async Register(phone_number: any) {
    let user: any = await this.http
      .post(`${ServerConfig.API}/api/auth/register`, phone_number)
      .toPromise();
    return user;
  }

  async Verify(code: any) {
    let token = await this.storage.get('token')
    console.log(code)
    let user: any = await this.http
      .post(`${ServerConfig.API}/api/auth/verify?access_token=${token}`, code)
      .toPromise();
    return user
  }
}
