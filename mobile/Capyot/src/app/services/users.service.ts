import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Storage } from '@ionic/storage';
import { ServerConfig } from '../configs/server.config';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(private http : HttpClient,
    private storage: Storage) { }

  async GetCurrent(){
    let token = await this.storage.get('token')
    return this.http.get(`${ServerConfig.API}/api/users/current?access_token=${token}`).toPromise()
  }
}
