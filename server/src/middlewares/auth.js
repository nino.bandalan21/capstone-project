import AuthBearer from 'hapi-auth-bearer-token';
import Tokens from '../modules/tokens';

export default class AuthMiddleware {

    static async Setup(server){
        await server.register(AuthBearer);
        server.auth.strategy('simple', 'bearer-access-token', {
            allowQueryToken: true,
            validate: async (request, token, h) => {
                let credentials = { token };
                let artifacts = null;
                let isValid = false;

                let _token = await Tokens.GetByToken(token);
                if(_token && _token.owner){
                    artifacts = _token.owner;
                    isValid = true;
                }
                return { isValid, credentials, artifacts };
            }
        });
    }
}