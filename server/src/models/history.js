import mongoose from "mongoose";

const Schema = mongoose.Schema;

const schema = new Schema({
  created_by: {
    type: Schema.Types.ObjectId,
    ref: "User"
  },
  action_type: {
    type: String
  },
  message: String,
  created_at: String,
});

module.exports = mongoose.model("History", schema);
