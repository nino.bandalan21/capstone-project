import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const schema = new Schema({
    message: String,
    created_at: {
        type: String,
        default: new Date()
    },
    status: {
        type: String,
        default: 'Created'
    }
})

module.exports = mongoose.model('Notification', schema);