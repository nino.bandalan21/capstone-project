import mongoose from "mongoose";

const Schema = mongoose.Schema;

const schema = new Schema({
  owner: { type: Schema.Types.ObjectId, ref: "User" },
  code: Number,
  token: String,
  is_verified: {
    type: Boolean,
    default: false
  },
  created_at: {
    type: Date,
    default: new Date()
  }
});

module.exports = mongoose.model("Token", schema);
