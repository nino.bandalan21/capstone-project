import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const schema = new Schema({
    type: String,
    code: String,
    routes: [{
        type: Schema.Types.ObjectId, ref: 'Route'
    }],
    update_status: String
})

module.exports = mongoose.model('Vehicle', schema);