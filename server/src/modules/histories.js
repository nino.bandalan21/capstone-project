import History from "../models/history";

export default class Histories {
  static async Save(data) {
    if (data._id) {
      let _id = data._id;
      delete data._id;
      return await History.update(
        {
          _id: _id
        },
        {
          $set: data
        }
      );
    } else {
      let history = new History(data);
      return await history.save();
    }
  }

  static async Listing(){
    return await History.find({}).populate('created_by')
  }
}
