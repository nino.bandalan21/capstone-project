import Route from "../models/route";

export default class Routes {
  static async Save(data) {
    //save the new Route or edit
    if (data._id) {
      let _id = data._id;
      delete data._id;
      return await Route.update(
        {
          _id: _id
        },
        {
          $set: data
        }
      );
    } else {
      let route = new Route(data);
      return await route.save();
    }
  }

  static async GetRoutes(vehicle_id) {}

  static async DeleteById(id) {
    return await Route.deleteOne({ _id: id });
  }

  static async GetRouteById(id) {
    return await Route.find({
      _id: id
    });
  }

  static async RoutesListing() {
    return await Route.find({});
  }
}
