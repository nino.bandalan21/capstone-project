import User from '../models/user';


export default class Users {

    static async FindByPhoneNumber(phone_number) {
        return await User.findOne({ phone_number: phone_number });
    }

    //Find Admin User
    static async FindByUserName(username) {
        return await User.findOne({ username: username });
    }


    static async Save(data) {
        if (data._id) {
            let id = data._id;
            delete data._id;
            return await User.update({
                _id: id
            }, {
                    $set: data
                });
        } else {
            let user = new User(data);
            return await user.save();
        }
    }

    static async GetCurrent(owner_id) {
        let user = await User.findOne({ _id: owner_id })
        return user
    }

    // static async Update(id, firstname, lastname){
    //     let _user = await User.findByIdAndUpdate({_id : id}, {$set: {firstname : firstname, lastname: lastname}})
    //     return _user;
    // }

    // static async GetAll(id){
    //     let _users = await User.find({_id : { $nin : (id)}})
    //     return _users
    // }
}