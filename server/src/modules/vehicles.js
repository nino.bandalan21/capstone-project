import Vehicle from '../models/vehicle'

export default class Vehicles {
    static async Save(data) {
        if (data._id) {
            let _id = data._id
            delete data._id
            return await Vehicle.update({
                _id: _id
            }, {
                    $set: data
                });
        } else {
            let vehicle = new Vehicle(data)
            return await vehicle.save()
        }
    }

    static async GetByCode(code) {
        let vehicle = await Vehicle.findOne({ code: code })
        return vehicle
    }

    static async GetById(id) {
        return await Vehicle.findOne({ _id: id }).populate('routes')
    }

    static async VehiclesListing(type) {
        let vehicles = null
        if (type) {
            vehicles = await Vehicle.find({ type: type })
        } else {
            vehicles = await Vehicle.find({}).populate('routes')
        }
        return vehicles
    }

    static async GetByRoute(route_id) {
        //i forgot the syntax
        //this is to find the Vehicle by route
    }
}