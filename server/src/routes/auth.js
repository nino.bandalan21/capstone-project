import Users from '../modules/users'
import Tokens from '../modules/tokens'
import Sms from '../modules/sms'
import md5 from 'md5'
import randomstring from 'randomstring'
import jwt from 'jsonwebtoken'

process.env.SECRET_KEY = 'secret'

export const AuthRoutes = [
    {
        method: 'POST',
        path: '/api/auth/register',
        handler: async (req, res) => {
            let data = {
                phone_number: req.payload.phone_number
            }
            let new_user = await Users.Save(data)
            if (new_user) {
                //generate the code to be verified in the verification page
                let code = randomstring.generate({
                    length: 5,
                    charset: 'numeric'
                })

                //generate token
                let token = randomstring.generate()
                let _token = await Tokens.Save({
                    code: code,
                    token: token,
                    owner: new_user._id
                })
                //generate response
                let _user = new_user.toObject()
                _user.token = _token
                //this is for the SMS later .. Needed to be discuss what API to use
                let message = 'Your verification code for CAPYOT is : ' + _token.code + '\n';
                // Sms.Send(_user.phone_number, message)
                return {
                    error: 0,
                    data: _user,
                    message: 'Successlly registered'
                }
            } else {
                return {
                    error: 1,
                    data: null,
                    message: 'Registering failed'
                }
            }
        }
    },
    {
        method: 'POST',
        path: '/api/auth/verify',
        config: {
            auth: 'simple'
        },
        handler: async (req, res) => {
            let code = req.payload.code
            console.log(code)
            let token = req.auth.credentials
            let _token = await Tokens.GetByToken(token.token)
            console.log(_token)
            if (_token) {
                if (_token.code === code) {
                    console.log('code matches')
                    let __token = _token.toObject()
                    __token.is_verified = true
                    console.log(__token)
                    let verified_token = await Tokens.Save(__token)
                    if (verified_token){
                        return {
                            error: 0,
                            data: __token,
                            message: 'Verification Success'
                        }
                    } else {
                        return {
                            error : 1,
                            data : null,
                            message: 'Cannot verify token'
                        }
                    }
                   
                } else {
                    return {
                        error: 1,
                        data: null,
                        message: 'Code does not match'
                    }
                }
            } else {
                return {
                    error: 1,
                    data: null,
                    message: 'Token is not authorized'
                }
            }
        }
    },
    {
        method: 'POST',
        path: '/api/auth/login',
        handler: async (req, res) => {
            let phone_number = req.payload.phone_number
            let password = req.payload.password
            let user = await Users.FindByPhoneNumber(phone_number)
            if (user) {
                //generate token
                let token = randomstring.generate()
                let _token = await Tokens.Save({
                    token: token,
                    owner: user._id,
                    is_verified : true
                })
                //generate response
                let _user = user.toObject()
                _user.token = _token
                return {
                    error : 0,
                    data : _user,
                    message : 'Successfully saved'
                }
            } else {
                return {
                    error: 1,
                    data: null,
                    message: 'Phone number not found'
                }
            }
        }
    },
    //API for admin user
    //Register Admin
    {
        method: 'POST',
        path: '/api/auth/admin/register',
        handler: async (req, res) => {
            let password = md5(req.payload.password)
            let data = {
                username: req.payload.username,
                password: password,
                url: req.payload.url
            }
            let user = await Users.FindByUserName(req.payload.username)
            if (user) {
                return {
                    error: 1,
                    data: user,
                    message: 'Admin Already Exists'
                }
            }
            else {
                let new_user = await Users.Save(data)
                if (new_user) {
                    let payload = {
                        id: new_user._id,
                        user: new_user
                    }
                    let token = jwt.sign(payload, process.env.SECRET_KEY, {
                        expiresIn: 1000
                    })
                    Tokens.Save({
                        token: token,
                        owner: new_user._id
                    })
                    var _data = { new_user, token }
                    return {
                        error: 0,
                        data: _data,
                        message: 'Success'
                    }
                }
            }
        }
    },
    //Login Admin
    {
        method: 'POST',
        path: '/api/auth/admin/login',
        handler: async (req, res) => {
            let username = req.payload.username
            let password = req.payload.password
            let user = await Users.FindByUserName(username)
            if (user) {
                let _password = md5(password)
                //match password
                if (user.password === _password) {

                    //yo yo code
                    // let payload = {
                    //     id: user._id,
                    //     user: user
                    // }
                    // let token = jwt.sign(payload, process.env.SECRET_KEY, {
                    //     expiresIn: 1000
                    // })
                    // var data = { user, token }

                    //generate token
                    let token = randomstring.generate()
                    let _token = await Tokens.Save({
                        token: token,
                        owner: user._id
                    })
                    //generate response
                    let _user = user.toObject()
                    _user.token = _token
                    return {
                        error: 0,
                        data: _user,
                        message: 'Login successfully'
                    }
                } else {
                    return {
                        error: 1,
                        data: null,
                        message: 'Password does not match'
                    }
                }
            } else {
                return {
                    error: 1,
                    data: null,
                    message: 'Username is not yet registered'
                }
            }
        }
    },
]