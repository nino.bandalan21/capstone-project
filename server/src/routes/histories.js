import Histories from '../modules/histories'

export const HistoriesRoutes = [
    {
        method : 'POST',
        path : '/api/history/save',
        config : {
            auth : 'simple'
        },
        handler : async (req,res) => {
            let user = req.auth.artifacts
            let action_type = req.payload.action_type
            let message = req.payload.message
            let created_at = req.payload.date
            let data = {
                created_by  : user._id,
                action_type : action_type,
                message : message,
                created_at : created_at
            }
            let history = await Histories.Save(data)
            if (history){
                return {
                    error : 0,
                    data : history,
                    message : 'History successfully saved'
                }
            } else {
                return {
                    error : 1,
                    data : null,
                    message : "Could not save history"
                }
            }
        }
    },
    {
        method : 'GET',
        path : '/api/history/listing',
        config : {
            auth : 'simple'
        },
        handler : async (req,res) => {
            let histories = await Histories.Listing()

            if (histories && histories.length > 0){
                return {
                    error : 0,
                    data : histories,
                    message : 'Success'
                }
            } else {
                return {
                    error : 1,
                    data : null,
                    message : 'Error'
                }
            }
        }
    }
]