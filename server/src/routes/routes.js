import Routes from '../modules/routes'
export const RoutesRoutes = [
    {
        method: 'POST',
        path: '/api/route/save',
        config: {
            auth: 'simple'
        },
        handler: async (req, res) => {
            let data = {
                _id: req.payload.id,
                lat: req.payload.lat,
                lng: req.payload.lng,
                landmark: req.payload.landmark
            }
            
            let route = await Routes.Save(data)
            if (route) {
                return {
                    error: 0,
                    data: route,
                    message: 'Route successfully saved'
                }
            } else {
                return {
                    error: 1,
                    data: null,
                    message: 'Error while saving the route'
                }
            }
        }
    },
    {
        method: 'POST',
        path: '/api/route/edit/{id?}',
        config: {
            auth: 'simple'
        },
        handler: async (req, res) => {
            //edit the route
        }
    },
    {
        method: 'GET',
        path: '/api/route/get/{id}',
        config: {
            auth: 'simple'
        },
        handler: async (req, res) => {
            let route = await Routes.GetRouteById(req.params.id)
            if (route) {
                return {
                    error: 0,
                    data: route,
                    message: 'Successfully getting the route'
                }
            } else {
                return {
                    error: 1,
                    data: null,
                    message: 'Can not find route'
                }
            }
        }
    },
    {
        method: 'GET',
        path: '/api/route/listing',
        config: {
            auth: 'simple'
        },
        handler: async (req, res) => {
            let routes = await Routes.RoutesListing()
            if (routes) {
                return {
                    error: 0,
                    data: routes,
                    message: 'Routes listing is successful'
                }
            } else {
                return {
                    error: 1,
                    data: null,
                    message: 'Error while getting the routes'
                }
            }
        }
    },
    {
        method : 'DELETE',
        path : '/api/route/delete',
        config : {
            auth : 'simple'
        },
        handler : async (req,res) => {
            let id = req.payload.id
            let deleted_route = await Routes.DeleteById(id)
            if (deleted_route){
                return {
                    error : 0,
                    data : deleted_route,
                    message : 'Successfully removed the route'
                }
            } else {
                return {
                    error : 1,
                    data : null,
                    message : 'Can not remove route.'
                }
            }
        }
    }
]